using System;
using System.Collections;
using System.Collections.Generic;
using Fusion;
using TMPro;
using UnityEngine;

public class Player : NetworkBehaviour
{
    private NetworkCharacterControllerPrototype _cc;
    [SerializeField] private Ball _prefabBall;

    [Networked] private TickTimer delay { get; set; }
    private Vector3 _forward;
    private TextMeshProUGUI _messages;

    
    [Networked(OnChanged = nameof(OnBallSpawned))]
    public NetworkBool spawned { get; set; }
    private Material _material;
    Material material
    {
        get
        {
            if(_material==null)
                _material = GetComponentInChildren<MeshRenderer>().material;
            return _material;
        }
    }

    private void Awake()
    {
        _cc = GetComponent<NetworkCharacterControllerPrototype>();
        _forward = transform.forward;
        _messages = FindObjectOfType<TextMeshProUGUI>();
    }

    private void Update()
    {
        if (Object.HasInputAuthority && Input.GetKeyDown(KeyCode.R))
        {
            RPC_SendMessage();
        }

        if (Input.GetKeyDown(KeyCode.T))
        {
            RPC_SendMessage2();
        }
    }

    [Rpc(RpcSources.InputAuthority, RpcTargets.All)]
    public void RPC_SendMessage(RpcInfo info = default)
    {
        Debug.Log(info.Source.PlayerId);
        spawned = !spawned;;
    }
    
    [Rpc(RpcSources.All, RpcTargets.InputAuthority)]
    public void RPC_SendMessage2(RpcInfo info = default)
    {
        if(info.IsInvokeLocal) return;
        Debug.Log(info.Source.PlayerId);
        spawned = !spawned;
    }

    public override void Spawned()
    {
        delay = TickTimer.CreateFromSeconds(Runner, 5f);
        PlayerData.Instance.SetPlayerData(Object.InputAuthority, this);
    }

    public static void OnBallSpawned(Changed<Player> changed)
    {
        changed.Behaviour.material.color = Color.white;
    }
    public override void Render()
    {
        material.color = Color.Lerp(material.color, Color.blue, Time.deltaTime );
    }
    public override void FixedUpdateNetwork()
    {
        if (GetInput(out NetworkInputData data))
        {
            data.direction.Normalize();
            _cc.Move(5 * data.direction * Runner.DeltaTime);

            if (data.direction.sqrMagnitude > 0)
                _forward = data.direction;

            if (delay.ExpiredOrNotRunning(Runner))
            {
                if ((data.buttons & NetworkInputData.MOUSEBUTTON1) != 0)
                {
                    // SpawnBall();
                }
            }
        }
    }

    public void SpawnBall()
    {
        delay = TickTimer.CreateFromSeconds(Runner, 0.5f);
        Runner.Spawn(_prefabBall,
            transform.position + _forward, Quaternion.LookRotation(_forward),
            Object.InputAuthority, (runner, o) =>
            {
                // Initialize the Ball before synchronizing it
                o.GetComponent<Ball>().Init();
            });
        spawned = !spawned;
    }
}