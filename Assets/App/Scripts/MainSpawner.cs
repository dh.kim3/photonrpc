using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
using TMPro;

public class MainSpawner : SimulationBehaviour, ISpawned
{
    #region Variable

    public NetworkPrefabRef rpcButton;
    public NetworkPrefabRef playerCharacter;
    public TextMeshProUGUI chatText;
    public Dictionary<PlayerRef, NetworkObject> _spawnedCharacters = new Dictionary<PlayerRef, NetworkObject>();

    private int _count = 0;

    #endregion

    #region FusionCallback

    public void Spawned()
    {
        if (Runner == null)
        {
            Runner = FindObjectOfType<NetworkRunner>();
        }

        SpawnButton(Runner.LocalPlayer);
        SpawnPlayer(Runner.LocalPlayer);
    }

    #endregion

    #region Method

    public void SpawnButton(PlayerRef player)
    {
        var button = Runner.Spawn(rpcButton, Vector3.zero, Quaternion.identity, player);
        // Runner.SetPlayerObject(player, button);
    }

    private void SpawnPlayer(PlayerRef player)
    {
        // Create a unique position for the player
        Vector3 spawnPosition =
            new Vector3((player.RawEncoded % 4) * 3, 1, (player.RawEncoded / 4) * 3);
        var playerObj = Runner.Spawn(playerCharacter, spawnPosition, Quaternion.identity, player);
        // Keep track of the player avatars so we can remove it when they disconnect
        // _spawnedCharacters.Add(player, networkPlayerObject);
    }

    #endregion
}