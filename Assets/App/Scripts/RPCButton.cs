using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fusion;
using TMPro;
using UnityEngine.UI;

public class RPCButton : NetworkBehaviour
{
    public TextMeshProUGUI textMesh;
    private TextMeshProUGUI _chat;

    private int _count;

    [Networked(OnChanged = nameof(OnNickNameChanged))]
    public NetworkString<_16> MyName { get; private set; }

    #region UnityCallbacks

    #endregion

    #region PhotonCallbacks

    public override void Spawned()
    {
        if (Object.HasInputAuthority)
        {
            if (string.IsNullOrWhiteSpace(PlayerData.Instance.myName) == false)
            {
                MyName = PlayerData.Instance.myName;
            }
        }

        PlayerData.Instance.SetButtonData(Object.InputAuthority, this);
        _chat = FindObjectOfType<MainSpawner>().chatText;
        var button = GetComponent<Button>();
        button.onClick.AddListener(() => { RPC_SendMessage(); });
    }

    public static void OnNickNameChanged(Changed<RPCButton> playerInfo)
    {
        if (string.IsNullOrWhiteSpace(playerInfo.Behaviour.MyName.ToString()))
        {
            return;
        }

        var debug = playerInfo.Behaviour.Object.Name + "\n";
        debug += playerInfo.Behaviour.Object.InputAuthority + "\n";
        debug += playerInfo.Behaviour.MyName + "\n";
        Debug.Log(debug);
        PlayerData.Instance.UpdateNickName(playerInfo.Behaviour.Object.InputAuthority,
            playerInfo.Behaviour.MyName.ToString());
    }

    #endregion


    public void SetPosition(ref int count)
    {
        var rect = GetComponent<RectTransform>();
        var canvas = FindObjectOfType<Canvas>();
        var h = rect.rect.height;
        transform.SetParent(canvas.transform);
        rect.anchorMax = new Vector2(0.5f, 1);
        rect.anchorMin = new Vector2(0.5f, 1);
        rect.anchoredPosition = new Vector2(0,
            -h/2 - 2 * h * count++);
    }

    public void SetText(string nickName)
    {
        textMesh.text = nickName;
        gameObject.name = nickName;
    }

    #region RPCs

    [Rpc(RpcSources.All, RpcTargets.All)]
    private void RPC_SendMessage(RpcInfo info = default)
    {
        _chat.text += MyName + /*"\t" +PlayerData.Instance.myName+ */"\n";
        if (MyName == PlayerData.Instance.myName)
        {
            PlayerData.Instance.players[Object.InputAuthority].SpawnBall();
        }
    }

    #endregion
}