using System;
using System.Collections;
using System.Collections.Generic;
using Fusion;
using Fusion.Sockets;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PhotonManager : MonoBehaviour, INetworkRunnerCallbacks
{
    #region Variable

    public TMP_InputField inputField;

    private NetworkRunner _runner;
    [SerializeField] private MainSpawner _mainSpawner;
    [SerializeField] private NetworkPrefabRef _playerPrefab;
    private Dictionary<PlayerRef, NetworkObject> _spawnedCharacters = new Dictionary<PlayerRef, NetworkObject>();
    private bool _mouseButton0;

    #endregion

    #region UnityCallback

    // Update is called once per frame
    private void Update()
    {
        _mouseButton0 = _mouseButton0 | Input.GetMouseButton(0);
    }

    private void OnGUI()
    {
        if (_runner == null)
        {
            if (GUI.Button(new Rect(0, 0, 200, 40), "Host"))
            {
                StartGame(GameMode.Host);
            }

            if (GUI.Button(new Rect(0, 40, 200, 40), "Join"))
            {
                StartGame(GameMode.Client);
            }

            if (GUI.Button(new Rect(0, 80, 200, 40), "Shared"))
            {
                StartGame(GameMode.Shared);
            }
        }
    }

    #endregion

    #region Method

    async void StartGame(GameMode mode)
    {
        _runner = gameObject.AddComponent<NetworkRunner>();
        _runner.ProvideInput = true;

        var myName = string.IsNullOrWhiteSpace(inputField.text)
            ? "player" + UnityEngine.Random.Range(0, 10000).ToString("0000")
            : inputField.text;
        PlayerData.Instance.SetNickName(myName);

        inputField.gameObject.SetActive(false);

        var args = new StartGameArgs()
        {
            GameMode = mode,
            SessionName = "TestRoom",
            Scene = SceneManager.GetActiveScene().buildIndex,
            SceneManager = gameObject.AddComponent<NetworkSceneManagerDefault>()
        };
        await _runner.StartGame(args);
    }

    #endregion


    #region NetworkRunnerCallback

    public void OnPlayerJoined(NetworkRunner runner, PlayerRef player)
    {
        {
            // // Create a unique position for the player
            // Vector3 spawnPosition =
            //     new Vector3((player.RawEncoded % runner.Config.Simulation.DefaultPlayers) * 3, 1, 0);
            // NetworkObject networkPlayerObject = runner.Spawn(_playerPrefab, spawnPosition, Quaternion.identity, player);
            // // Keep track of the player avatars so we can remove it when they disconnect
            // _spawnedCharacters.Add(player, networkPlayerObject);
        }
    }

    public void OnPlayerLeft(NetworkRunner runner, PlayerRef playerRef)
    {
        var playerData = PlayerData.Instance;
        Debug.Log(playerData.names[playerRef] + " is Left");
        
        runner.Despawn(playerData.players[playerRef].Object);
        playerData.players.Remove(playerRef);

        if (PlayerData.Instance.buttons.TryGetValue(playerRef, out var button))
        {
            runner.Despawn(button.Object);
            playerData.buttons.Remove(playerRef);
            playerData.names.Remove(playerRef);
        }

        // if (_spawnedCharacters.TryGetValue(player, out NetworkObject networkObject))
        // {
        //     runner.Despawn(networkObject);
        //     _spawnedCharacters.Remove(player);
        // }
    }

    public void OnInput(NetworkRunner runner, NetworkInput input)
    {
        var data = new NetworkInputData();

        var v = Input.GetAxis("Vertical");
        var h = Input.GetAxis("Horizontal");

        data.direction += new Vector3(h, 0, v);

        if (_mouseButton0)
            data.buttons |= NetworkInputData.MOUSEBUTTON1;
        _mouseButton0 = false;

        input.Set(data);
    }

    public void OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input)
    {
    }

    public void OnShutdown(NetworkRunner runner, ShutdownReason shutdownReason)
    {
    }

    public void OnConnectedToServer(NetworkRunner runner)
    {
        _mainSpawner.Spawned();
    }

    public void OnDisconnectedFromServer(NetworkRunner runner)
    {
    }

    public void OnConnectRequest(NetworkRunner runner, NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token)
    {
    }

    public void OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress, NetConnectFailedReason reason)
    {
    }

    public void OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message)
    {
    }

    public void OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList)
    {
    }

    public void OnCustomAuthenticationResponse(NetworkRunner runner, Dictionary<string, object> data)
    {
    }

    public void OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken)
    {
    }

    public void OnReliableDataReceived(NetworkRunner runner, PlayerRef player, ArraySegment<byte> data)
    {
    }

    public void OnSceneLoadDone(NetworkRunner runner)
    {
    }

    public void OnSceneLoadStart(NetworkRunner runner)
    {
    }

    #endregion
}