using System;
using System.Collections;
using System.Collections.Generic;
using Fusion;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerData : MonoBehaviour
{
    private static PlayerData _instance;

    public static PlayerData Instance => _instance;

    public int count = 0;

    public string myName;
    public Dictionary<PlayerRef, RPCButton> buttons = new Dictionary<PlayerRef, RPCButton>();
    public Dictionary<PlayerRef, Player> players = new Dictionary<PlayerRef, Player>();
    public Dictionary<PlayerRef, string> names = new ();

    public Dictionary<PlayerRef, ClassSet> Sets = new();
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetNickName(string nickName)
    {
        myName = nickName;
    }
    public void SetButtonData(PlayerRef player, RPCButton button)
    {
        if (buttons.ContainsKey(player)) return;
        buttons.Add(player, button);
        names.Add(player, button.MyName.ToString());
        buttons[player].SetPosition(ref count);
    }

    public void SetPlayerData(PlayerRef playerRef, Player player)
    {
        if(players.ContainsKey(playerRef)) return;
        players.Add(playerRef, player);
    }

    public void UpdateNickName(PlayerRef player, string nickName)
    {
        if(buttons.TryGetValue(player, out var button) == false) return;
        names[player] = nickName;
        button.SetText(nickName);
    }
}

public class ClassSet
{
    public RPCButton rpcButton;
    public Player player;
}